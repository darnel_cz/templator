templator
=========

.. _Python 3: https://www.python.org/
.. _Jinja 2: http://jinja.pocoo.org/docs/dev/
.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _Tex: https://www.tug.org/begin.html
.. _LaTeX: https://www.latex-project.org/
.. _virtualenv: https://virtualenv.pypa.io/en/stable/
.. _pyyaml: http://pyyaml.org/wiki/PyYAMLDocumentation

A tool for creating documents combining a template and a context (configuration).
It is written in `Python 3`_ and utilizes `Jinja 2`_ to process templates. It is 
know to work with `Restructured Text`_ and `LaTeX`_ (or `TeX`_ in general).

Installation
------------

The tool uses virtualenv_ to create a runtime environment.

  #. Install virtualenv_ through package manager of your operating system. 
  #. Create virtualenv in the project directory. ::

     cd path/to/project
     virutalenv -p /path/to/python3 .venv

  #. Activate virtualenv. ::

     source .venv/bin/activate

  #. Download project dependencies. ::

     pip install -r requiremnts.txt

Usage
-----

.. warning:: Don't forget to activate virtualenv_ prior running the tool.

Configuration
~~~~~~~~~~~~~

A context is a data structure to render a *template* with. It must be valid YAML
file containing arbitrary information. You can use any syntax supported by pyyaml_
(e.g. references) as show in the following example. ::

  persons:
    john: &author
      name: John Example

  document:
    title: Example document
    author: *author

.. _FilesystemLoader: processor/output.py

*Template* is a document to render. It may contain expressions that will be
evaluated using *Context*. *Templates* are processed by `Jinja 2`_. It is possible
to use all of its features including inheritance (``extend``) or including
(``include``) other template files. These template files must be located on the
same path as the main template (thus FilesystemLoader_ is able to find them). ::

  # {{document.title}}

  {{author.name}}

  Text ...

Jinja2
______

If TeX_, LaTeX_ or similar are used as output format `Jinja 2`_ default tokens
denoting start/end of a block or variable may clash with directives and macros.
It is necessary to redefine them. It is possible to provide a configuration
to templator. It is provided as a INI style configuration file. An example is
shown bellow. ::

  [jinja2]
  block_start_string = \%%{
  block_end_string = }
  variable_start_string = \*{
  variable_end_string = }
  comment_start_string = \#{
  comment_end_string = }

Please note that no configuration is needed if default tokens are used.

Invocation
~~~~~~~~~~

::

  Usage: templator.py run [OPTIONS] TEMPLATE CONTEXTS...

    Outputs a template rendered using contexts to STDOUT.

    CONTEXTS is one or more files/directories. If a directory is given it is
    searched for *yml files recursively. All files are loaded in the same
    order as they appear on the commandline or based on their order in
    directories.

  Options:
    --config TEXT                   Path to configuration file
    --dump / --no-dump              Dump context to STDERR
    --expressions / --no-expressions
                                    Parse Jinja2 expressions in context or not
    --schema TEXT                   Validate resulting context using pykwalify
                                    schema. May be provided multiple times.
    --scope-root TEXT               If given attributes are scoped using
                                    directory path left between scope root and
                                    file.
    --root TEXT                     Set context root
    --help                          Show this message and exit.

.. _examples: examples/README.rst

Look into `examples`_ folder for further reference.
