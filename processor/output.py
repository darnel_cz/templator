# processor/output.py
# Classes handling output processing

from jinja2 import BaseLoader, TemplateNotFound
from os.path import join, exists, getmtime
from processor.common import FileReader


class FilesystemLoader(BaseLoader):
    """
    Template loader utilizing filesystem
    """

    def __init__(self, filename):
        self.filename = filename

    def get_source(self, environment, template):
        path = join(self.filename, template)
        if not exists(path):
            raise TemplateNotFound(template)
        mtime = getmtime(path)
        with FileReader(path) as f:
            #source = f.read().decode('utf-8')
            source = f.read()
        return source, path, lambda: mtime == getmtime(path)
