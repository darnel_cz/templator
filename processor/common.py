# processor/common.py
# Common constructs


class FileReader(object):
    """
    Opens and reads a file
    """
    def __init__(self, filename):
      self.file = open(filename)

    def __enter__(self):
        return self.file

    def __exit__(self, ctx_type, ctx_value, ctx_traceback):
        self.file.close()
