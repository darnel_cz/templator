Examples
========

This sections contains examples how to use the tool.

.. _01 - Basic usage: 01-basic_usage/README.rst
.. _02 - LaTeX output: 02-latex_output/README.rst

  * `01 - Basic usage`_
  * `02 - LaTeX output`_

