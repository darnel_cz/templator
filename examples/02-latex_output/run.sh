#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" run --dump --config "$SCRIPT_DIR/config.ini" "$SCRIPT_DIR/template.tex.j2" "$SCRIPT_DIR/context.yml" > "/tmp/02-latex_output.tex"

deactivate
