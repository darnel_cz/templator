#!/bin/sh

SCRIPT_DIR=$(dirname "$0")


. "$SCRIPT_DIR/../../.venv/bin/activate"
python "$SCRIPT_DIR/../../templator.py" run --schema "$SCRIPT_DIR/schema.yml" --dump "$SCRIPT_DIR/template.rst.j2" "$SCRIPT_DIR/context_invalid.yml" > "/tmp/04-schema_validation.rst"

echo "\nReturn code: $?"

deactivate
