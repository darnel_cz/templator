01 - Basic usage
================

.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _context.yml: context.yml
.. _template.rst.j2: template.rst.j2
.. _run.sh: run.sh

This is a basic demonstration how to use templator with `Restructured Text`_.

  * context - context.yml_
  * template - template.rst.j2_

Use run.sh_ to try this example. It starts virtualenv, invokes templator, stores
result in ``/tmp/01-basic_usage.rst`` and deactivates virtualenv.
