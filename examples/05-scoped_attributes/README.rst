05 - Scoped attributes
======================

.. _Restructured Text: http://docutils.sourceforge.net/rst.html
.. _template.rst.j2: template.rst.j2
.. _run.sh: run.sh

This is a demonstration how to use scoped attributed. Attributes are structured 
in a tree strucure that reflects directory paths to particular files.

  * context (directory, the tree root)
    
    * context_root.yml (a file, all attributes are direct descendants of the tree root)
    * section1 (directory, constitutes a scope ``section1``)

      * context.yml (a file, all attributes are descendants of ``section1``

    * section2 (directory, constitutes a scope ``section2``)

      * context.yml (a file, all attributes are descendants of ``section2``

Use run.sh_ to try this example. It starts virtualenv, invokes templator, stores
result in ``/tmp/05-scoped_attributes.rst`` and deactivates virtualenv.
