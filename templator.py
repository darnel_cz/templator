#!/usr/bin/env python
# Executes commands

import click
import configparser
from jinja2 import Environment
from pykwalify.errors import SchemaError
from os.path import abspath, dirname, basename, exists
import sys
import yaml


from processor.input import parse, get_context
from processor.output import FilesystemLoader


def configure(config, key):
    if config is not None:
        c = configparser.ConfigParser()
        c.read(abspath(config))
        if key in c.sections():
            return c[key]
    return {}


@click.group()
def cli():
    sys.path.append(abspath(dirname(sys.argv[0])))


@cli.command()
@click.argument('template')
@click.argument('contexts', nargs=-1, required=True)
@click.option('--config', default=None, help="Path to configuration file")
@click.option('--dump/--no-dump', default=False, help="Dump context to STDERR")
@click.option('--expressions/--no-expressions', default=True, help="Parse Jinja2 expressions in context or not")
@click.option('--schema', default=None, multiple=True,
              help="Validate resulting context using pykwalify schema. May be provided multiple times.")
@click.option('--scope-root', default=None, multiple=True,
              help="If given attributes are scoped using directory path left between scope root and file.")
@click.option('--root', default=None, help="Set context root")
def run(contexts, template, config, dump, expressions, schema, scope_root, root):
    """
    Outputs a template rendered using contexts to STDOUT.

    CONTEXTS is one or more files/directories. If a directory is given it is
    searched for *yml files recursively. All files are loaded in the same order
    as they appear on the commandline or based on their order in directories.
    """
    config = configure(config, 'jinja2')
    env = Environment(
        loader=FilesystemLoader(dirname(template)),
        **config
    )
    template = env.get_template(basename(template))
    try:
        context = get_context(contexts, expressions, schema, scope_root, root)
        if dump:
            click.echo(yaml.dump(context), err=True)

        click.echo(template.render(context))
    except SchemaError as ex:
        click.echo("\n{}".format(ex.msg), err=True)
        exit(1)


if __name__ == '__main__':
    cli()
